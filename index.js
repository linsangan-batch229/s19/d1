//if, else if, else statement

let numG = -1;

//if 

if (numG > 0) {
    console.log('hekki');
} else if (numG < 0){
    console.log('wolrd');
} else {
    console.log('WOW');
}

let msg = "";

function typhoonIntensity(windSpd){

    if(windSpd < 30){
        return 'Not a typhoon yet!';
    } else if (windSpd <= 61){
        return 'Tropical depression detected';
    } else if (windSpd >= 62 && windSpd <= 88){
        return 'Tropical storm detected';
    } else if (windSpd >= 89 && windSpd <= 117){
        return 'Severed tropical Storm detected';
    } else {
        return 'Typhoon detected';
    }
}

msg = typhoonIntensity(70);
console.log(msg);

if(msg === 'Tropical storm detected'){
    console.warn(msg);

msg = typhoonIntensity(112);
console.log(msg);

msg = typhoonIntensity(120);
console.log(msg);


}

//Truthy Falsy
///* 
// - In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean context
// - Values are considered true unless defined otherwise
// - Falsy values/exceptions for truthy:
//     1. false
//     2. 0
//     3. -0
//     4. ""
//     5. null
//     6. undefined
//     7. NaN
// */

if (true){
    console.log('Truthy');  
}

if (1){
    console.log('Truthy');  
}

if ([]){
    console.log('Truthy');  
}

//Ternary operator
//or conditional rendering on FS
/* 
- The Conditional (Ternary) Operator takes in three operands:
    1. condition
    2. expression to execute if the condition is truthy
    3. expression to execute if the condition is falsy
- Can be used as an alternative to an "if else" statement
- Ternary operators have an implicit "return" statement meaning that without the "return" keyword, the resulting expressions can be stored in a variable
- Commonly used for single statement execution where the result consists of only one line of code
- For multiple lines of code/code blocks, a function may be defined then used in a ternary operator
- Syntax
    (expression) ? ifTrue : ifFalse;
    a < 1 ? ifTrue : ifFalse;
*/

//sample of ternary operattor
let num1 = (1 < 18) ? true : false;
console.log(num1);

//multiple statement execution
//both function perform two separate tasks

let name;

function isOfLegalAge(){
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge(){
    name = 'Jane';
    return "You're under the age limit";
}

// let age = parseInt(prompt("What is your age?"));
// console.log(typeof age);
// console.log(age);

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result: " + legalAge + ", " + name);

// Switch statements
//detailed if statement

/* 
- Can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected input
- The ".toLowerCase()" function/method will change the input received from the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercased letters
- The "expression" is the information used to match the "value" provided in the switch cases

- Variables are commonly used as expressions to allow varying user input to be used when comparing with switch case values
- Switch cases are considered as "loops" meaning it will compare the "expression" with each of the case "values" until a match is found
- The "break" statement is used to terminate the current loop once a match has been found
- Removing the "break" statement will have the switch statement compare the expression with the values of succeeding cases even if a
match was found
- Syntax
    switch (expression) {
        case value:
            statement;
            break;
        default:
            statement;
            break;
    }
*/


// let day = prompt('What day of the week is it today?').toLowerCase();
// console.log(day);


// switch (day){
//     case 'monday':
//         console.log('Color day is red');
//         break;

//     case 'tuesday':
//         console.log('Color day is blue');
//         break;

//     default :
//         console.log('input valid date');
//         break;
// }

// Try-Catch-finally statement

function showIntensityAlert(windSpd){

    try {
        //attempt to execute a code
        alerat(typhoonIntensity(windSpd));
    } catch (error) {
        console.log(typeof error);
        console.warn(error.message);
    } finally {
        alert('Intensity updates will show new alert');
    }

}

showIntensityAlert(56);